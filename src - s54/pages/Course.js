import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

export default function Course() {

	console.log(courseData);
	console.log(courseData[0]);

	const courses = courseData.map(course => {
		return (
			<CourseCard key={course.id} course={course}/>
		)
	  }
	)

	return (
		<>
		<h1>Courses</h1>
			{courses}
		</>

	)
  }
