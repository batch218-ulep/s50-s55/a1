import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

export default function CourseCard({course}) {
	//This will check if our data is passed successfully.
	// console.log(props);
	// console.log(typeof props);

	//Destructured Vaiable from courseProp
	const { name, description, price } = course;

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [disabled, setDisabled] = useState(false)
	// const [isOpen, setIsOpen] = useState(true)

	console.log(useState(0));

	// Activity s51
	function enroll() {
		if (seats > 0) {
			setCount(count + 1);
			console.log("Enrollees: " + count);

			setSeats(seats - 1);
			console.log("seats: " + seats);
		} else {
			alert("No more seats available!");
		}
	}


	// function enroll() {
	// 	setCount(count + 1);
	// 	console.log("Enrollees: " + count);
	// }
	// document.querySelector('#btn-enroll-${id}').setAttribute('disabled', true);

	CourseCard.propTypes = {
		course: PropTypes.shape({
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired
		})
	}

	useEffect(() => {
		if (seats === 0) {
			setDisabled(true)
			// setIsopen(false)
			alert("No more Seats!")
		}
	}, [seats])



	return (
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>

				<Card.Subtitle>Seats: {seats}</Card.Subtitle>
				<Card.Text>Enrollees: {count}</Card.Text>
				<Button disabled={disabled} classname="bg-primary" onClick={enroll}>Enroll</Button>

			</Card.Body>
		</Card>
	)
}

// Activity s50
// export default function CourseCard() {
// 	return (
// 	<Card>
// 	    <Card.Body>
// 	        <Card.Title>Sample Course</Card.Title>
// 	        <Card.Subtitle>Description:</Card.Subtitle>
// 	        <Card.Text>This is a sample course offering.</Card.Text>
// 	        <Card.Subtitle>Price:</Card.Subtitle>
// 	        <Card.Text>PhP 40,000</Card.Text>
// 	        <Button variant="primary">Enroll</Button>
// 	    </Card.Body>
// 	</Card>
// 	)
// }
