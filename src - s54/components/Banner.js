import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({banner}) {

	console.log(banner)
	const { title, content, destination, label } = banner

	return (
		<Row>
			<Col className="p-5 text-right">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>


	)
}
