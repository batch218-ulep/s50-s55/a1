import Banner from '../components/Banner';
// import { Link } from 'react-router-dom';

export default function Error() {

  const data = {
    title: "Page Not Found",
    content: "Go back to",
    destination: "/",
    label: "Homepage"
  }

  console.log(data)
  return (
    <>
    	<Banner banner={data}/>
	</>
  )
}

