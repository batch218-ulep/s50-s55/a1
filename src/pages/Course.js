// import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

import { useState, useEffect} from 'react';

export default function Course() {

	// console.log(courseData);
	// console.log(courseData[0]);

	const [courses, setCourses] = useState([]);

	// const courses = courseData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course={course}/>
	// 	)
	//   }
	// )

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				<CourseCard key={course.id} course={course}/>			
			}));
		})
	}, []);

	return (
		<>
		<h1>Courses</h1>
			{courses}
		</>
	)
  }
