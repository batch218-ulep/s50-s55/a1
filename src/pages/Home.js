import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'


export default function Home() {
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for Everyone, Everywhere",
		destination: "/courses",
		label: "Enroll Now!"
	}

	return (
		<>
			<Banner banner={data} />
			<Highlights />
		</>
	)
}
