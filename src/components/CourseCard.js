import { Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';

export default function Course({course}) {

	const { name, description, price, _id } = course;

	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);
	// const [disabled, setDisabled] = useState(false)

	// console.log(useState(0));

	// function enroll() {

	// 	if (seats > 0) {
	// 		setCount(count + 1);
	// 		console.log("Enrollees: " + count);

	// 		setSeats(seats - 1);
	// 		console.log("seats: " + seats);
	// 	} else {
	// 		alert("No more seats available!");
	// 	}
	// }


	CourseCard.propTypes = {
		course: PropTypes.shape({
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired
		})
	}

	// useEffect(() => {
	// 	if (seats === 0) {
	// 		setDisabled(true)
	// 		// setIsopen(false)
	// 		alert("No more Seats!")
	// 	}
	// }, [seats])



	return (
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>

				{/*<Card.Subtitle>Seats: {seats}</Card.Subtitle>*/}
				{/*<Card.Text>Enrollees: {count}</Card.Text>*/}
				<Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
			</Card.Body>
		</Card>
	)
}


