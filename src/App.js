// import logo from './logo.svg';
import './App.css';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';

import Home from './pages/Home';
import Register from './pages/Register';
import Course from './pages/Course';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from "./pages/Error";

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'


export default function App() {

  // const [user, setUser] = useState({email:localStorage.getItem("email")});

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {localStorage.clear()
  };

  useEffect(() => {
      fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
          // if(data._id !== undefined) {
          if(typeof data._id !== "undefined") {
              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          } 
          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }
      })
  }, []);


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>
            <Route  path="/" element={<Home/>} />
              <Route  path="/courses" element={<Course/>} />
              <Route  path="/courses/:courseId" element={<CourseView/>} />
              <Route  path="/login" element={<Login/>} />
              <Route  path="/register" element={<Register/>} />
              <Route  path="/logout" element={<Logout/>} />
              <Route path="*" element={<Error/>} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}
